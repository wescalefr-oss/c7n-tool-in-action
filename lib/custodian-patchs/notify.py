--- tmp/notify-old.py	2020-05-03 16:25:32.000000000 +0200
+++ tmp/notify-new.py	2020-05-03 16:21:49.000000000 +0200
@@ -16,7 +16,9 @@
 import base64
 import copy
 import zlib
+import json
 
+from botocore.vendored import requests
 from .core import EventAction
 from c7n import utils
 from c7n.exceptions import PolicyValidationError
@@ -144,7 +146,14 @@
                      'properties': {
                          'topic': {'type': 'string'},
                          'type': {'enum': ['sns']},
-                         'attributes': {'type': 'object'},
+                         'attributes': {'type': 'object'}}},
+                    {'type': 'object',
+                     'required': ['type', 'url'],
+                     'properties': {
+                         'url': {'type': 'string'},
+                         'type': {'enum': ['slack']},
+                         'channel': {'type': 'string'},
+                         'attributes': {'type': 'object'}
                      }}]
             },
             'assume_role': {'type': 'boolean'}
@@ -231,6 +240,27 @@
             return self.send_sqs(message)
         elif self.data['transport']['type'] == 'sns':
             return self.send_sns(message)
+        elif self.data['transport']['type'] == 'slack':
+            return self.send_slack(message)
+
+    def send_slack(self, message):
+        url = self.data['transport']['url'].format(**message)
+        channel = self.data['transport']['channel'].format(**message)
+        region = message['region']
+        description = message['policy']['description']
+        body = f"{description}: {region}, on account : {message['account_id']}"
+        data = {
+            "text": f"SécuBot alarm raised at {message['event']['detail']['eventTime']}",
+            "channel": channel,
+            "username": "SecuBot",
+            "attachments": [
+                {
+                    "color": "#f41308",
+                    "text": body
+                }
+            ]
+        } 
+        requests.post(url = url, data = json.dumps(data))
 
     def send_sns(self, message):
         topic = self.data['transport']['topic'].format(**message)
