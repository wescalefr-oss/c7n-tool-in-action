# Cloud custodian Tool In Action

This project permit to setup a minimal configuration for multi region security deployment with some custodian rules.

## Getting Started

You need to have a good knowlege on terraform and cloudcustodian tu use this repository, currently supporting in Python3.7. In order to run this repo properly, you need to operate on UNIX system (you may need to do little adaptations on Winsows)

### Prerequisites

First of all you need to have terraform12 binary on path.

```
terraform --version
> Terraform v0.12.24
```

You also need a vritual env named 'env' in the root directory, after cloning the repo, navigate through root folder and run 

```
python3 -m venv env
. ./env/bin/activate
```

You also need to have set up your aws credentials with an aws profile

```
cat ~/.aws/config
> [profile sbx]
> region=eu-west-1
> aws_access_key_id=XXX
> aws_secret_access_key=XXX
```

### Installing

The first step is to install the requirements

```
pip install -r requirements.txt
```

Then complete your configuration like

```
cat conf/config.json

{
    "profile": "sbx", <- Your AWS profile
    "provider_version": "3.46", <- AWS provider version
    "registration": {
        "region": "us-east-1" <- Main deployment region
    },
    "regions_to_protect": [ <- List of regions where the rules will be recursively deployed
        "eu-west-1", 
        "eu-west-2"
    ]
}
```

## Deploying the infrastructure

Simply run 

```sh
python init.py --account_id YOUR_ACCOUNT_ID
```

### Develop a custodian patch

First do your modifications in cloud custodian library
and keep a unmodified file aside then run 

```sh
diff -uBa <old_file> <new_file> > "patched_file.py"
```

and modify the script patch.sh according to your modification:

```sh
echo "Patching custodian"
to_patch="${folder}/env/lib/python3.7/site-packages/c7n/actions/notify.py"
patched="${folder}/lib/custodian-patchs/notify.py"
patch --no-backup-if-mismatch -u -s -N --verbose ${to_patch} -i ${patched}
```

your modifications will be automaticly patched at each deployment

## Removing

Simply run 

```sh
python remove.py --account_id YOUR_ACCOUNT_ID
```