resource "aws_iam_role" "CustodianRole" {
    name="SEC-CustodianCore-global"
    path="/SECURITY/"

    assume_role_policy = <<-EOF
    {
        "Version": "2012-10-17",
        "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
            "Service": "lambda.amazonaws.com"
            },
            "Effect": "Allow"
        }
        ]
    }
    EOF
}

resource "aws_iam_role_policy" "CustodianRolePolicy" {
    name = "SEC-CustodianCore-global-policy"
    role = aws_iam_role.CustodianRole.id

    policy = <<-EOF
    {
        "Version": "2012-10-17",
        "Statement": [
        {
            "Action": [
            "s3:*",
            "ec2:Describe*",
            "sns:Publish",
            "iam:ListAccountAliases"
            ],
            "Effect": "Allow",
            "Resource": "*"
        }
        ]
    }
    EOF
}

resource "aws_iam_role_policy_attachment" "lambdaExecutionRole" {
    role = aws_iam_role.CustodianRole.name
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

